﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicArmenia.Lesson._001_Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st = new Student();

            int code = st.GetHashCode();
            Type type = st.GetType();
            string text = st.ToString();
        }
    }
}
